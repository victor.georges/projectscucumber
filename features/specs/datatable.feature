#language: pt

Funcionalidade: Data table

Cenario: Cortar laranjas
Dado  porto varias naranjas
|laranja| 10|
Quando eu cortar 2 naranjas
Então constato numero das laranjas 


Cenario: Consumir laranjas
Dado eu tenho algumas laranjas
|laranja|
|10     |
Quando eu consumo 2 laranjas
Então eu verifico quantas há
