#language: pt
@funcio
Funcionalidade: Multiplicação

Esquema do Cenario: Realizar uma multiplicação
Quando eu realizo minha multiplicação de minhas <laranjas> por um <valor>.
Então quero descobrir o <resultado>.

Exemplos:

|laranjas|valor|resultado|
|10    |5    |50      |
|10    |3    |30      |
|10    |2    |20      |
|10    |10   |100     |


