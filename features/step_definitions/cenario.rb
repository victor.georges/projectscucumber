Dado('Eu tenho {int} laranjas') do |valor1|
    @laranjas=valor1
end
    
Quando('Eu comer {int} laranja') do |valor2|
    @comer=valor2
    @restaram=@laranjas-@comer
    print @restaram
end
    
Então('Vejo quantas laranjas terei') do 
    expect(@restaram).to eq 8
    print @restaram
end