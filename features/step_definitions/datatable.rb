Dado('porto varias naranjas') do |table|
    @laranja= table.rows_hash['laranja'].to_i
end

Quando('eu cortar {int} naranjas') do |valor1|
    @cortar=valor1
    @resultado=@laranja-@cortar
    print @resultado
end

Então('constato numero das laranjas ') do |verificando|
    expect(@resultado).to eq verificando
end

Dado('eu tenho algumas laranjas') do |table1|
    table1.hashes.each do |valor|
        @laranjas = valor['laranja'].to_i
    end
end

Quando('eu consumoo {int} laranjas') do |valor2|
    @consumo=valor2
    @total=@laranjas-@consumo
    print @total
end

Então('eu verifico quantas há') do |totalizando|
    expect(@total).to eq totalizando
end