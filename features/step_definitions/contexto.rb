
Dado('que eu tenho {int} laranjas na bolsa.') do |valor|
    @laranjas=valor
end

Quando('eu coloco {int} laranjas na bolsa') do |valor1|
    @coloquei=valor1
    @laranjas_colocada=@coloquei+@laranjas
    print @laranjas_colocada
end

Então('eu verifico se o total de laranjas na bolsa é {int}') do |total1|
    expect(@laranjas_colocada).to eq total1
end

Quando('eu tiro {int} laranjas da bolsa') do |valor3|
    @retirado=valor3
    @laranjas_retiradas=@laranjas-@retirado
    print @laranjas_retiradas
    
end

Então('eu verifico se o total de laranjas na bolsa é {int}') do |total|
    expect(@laranjas_retiradas).to eq total
end