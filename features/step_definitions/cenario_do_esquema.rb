Quando('eu realizo minha multiplicação de minhas {int} por um {int}.') do |laranjas, valor1|
    @laranjas_multiplicacao=laranjas*valor1
    print @laranjas_multiplicacao
end

Então('quero descobrir o {int}.') do |valor|
    expect(@laranjas_multiplicacao).to eq valor
end